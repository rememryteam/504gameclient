﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Net;
using System.IO;
using UnityEngine.Networking;
using Ionic.Zip;
using Mono.Data.Sqlite;
using System.Data;
using System;
using System.Collections.Generic;

/**
 * This class is the first code to run and does the following tasks:
 * 1. initializing db connection and providing needed methods and varialbes during the game.
 * 2. providieing other methods and variables needed all during the game
 * Note that this script never dies!
 * */
public class HttpResponse{
	public string text;
	public bool success;

	public HttpResponse(){
		success = false;
		text = "";
	}
}
public class User{
	public string id, name, level, score;
	public int[] badges;
	public bool IFollowHim;
	public User(){
	}
	public User(string iid, string iname, string ilevel,string iScore, bool ifh){
		id = iid;
		name = iname;
		level = ilevel;
		score = iScore;
		IFollowHim = ifh;
	}
	public override string ToString(){
		return "id: " + id + ", name: " + name + ", level: " + level + ", score: " + score+", I follow him:"+IFollowHim;
	}
}
public class codeMother : MonoBehaviour {
	public string baseURL;
	public static codeMother cmInstance;

	public SqliteConnection con_db;
	public SqliteCommand cmd_db;
	public SqliteDataReader rdr;
	private string dbPath;
	public string response;
	public string loginID, username;
	public string Auth;
	public string myID;
	public string[] loginResponse;

	/**
	 * friend is the info of a player whom the user wants to see profile of. Because of unity's limitations about passing arguments to scenes, this has to be handled here
	 * */
	public User friend;
	List<int> badges;
	// Use this for initialization
	void Start () {		
//		baseURL = "http://172.30.26.209/gameof504server/index.php/";
		baseURL = "/gameof504server/index.php/";
		Debug.Log ("codeMother Starting");
		cmInstance = this;
		DontDestroyOnLoad (transform.gameObject);
		badges = new List<int> ();
		friend = new User ();

	}
	public SqliteConnection connectDB() {
		try{
			if(Application.platform != RuntimePlatform.Android){
				dbPath = Application.dataPath + "/db/rememryClient.bytes.db";	
			} else {
				dbPath = Application.persistentDataPath + "/db/rememryClient.bytes.db";	
				if (!File.Exists(dbPath))
				{
					WWW load = new WWW("jar:file://" + Application.dataPath + "!/assets/" + "zordor.db");
					while (!load.isDone) { }
					File.WriteAllBytes(dbPath, load.bytes);
				}
			}
			con_db = new SqliteConnection ("URI=file:"+dbPath);
			con_db.Open ();
			if (con_db.State == ConnectionState.Open) {
				Debug.Log (dbPath + " -connected");
			}
		} catch(Exception ex){
			Debug.Log ("Connection failed:" + ex);
		}
		return con_db;
	}

	public void disconnectDB(SqliteDataReader reader,SqliteConnection conn,SqliteCommand command)
	{
		reader.Close();
		reader = null;
		command.Dispose();
		command = null;
		conn.Close();
		conn = null;
		GC.Collect();
		Debug.Log("Disconnected from db.");
	}

	public IEnumerator sendHttpRequest(string controller, string action, string[] parameters,HttpResponse result) {
		string request = baseURL + controller +"/" + action;
		if (parameters.Length > 0)
			request += "?";
		int i = 0;
		foreach (string p in parameters) {
			request += p;
			i++;
			if (parameters.Length - i >= 1)
				request += "&";
		}
//		Debug.Log ("request is:" + request); 
		yield return StartCoroutine(sendRequestRoutine(request,result));
	}

	private IEnumerator sendRequestRoutine(string url, HttpResponse result)
	{				
//		char[] separatingChars = { ','};  
		UnityWebRequest www = UnityWebRequest.Get(url);
		Debug.Log("URL: "+www.url);
		yield return www.Send();

		if (www.isError) {
			Debug.Log ("ERROR " + www.error);
			result.success = false;
		} else {
			response = www.downloadHandler.text;
			Debug.Log ("SUCCESS " + response);
			result.success = true;
		}

		result.text = response;
	}
	public IEnumerator sendRequestRoutinePost(string url, HttpResponse result)
	{				
		//		char[] separatingChars = { ','};  
		List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
		formData.Add( new MultipartFormDataSection("an=using") );
		UnityWebRequest www = UnityWebRequest.Post(url,formData);
		Debug.Log("URL: "+www.url);
		yield return www.Send();

		if (www.isError) {
			Debug.Log ("ERROR " + www.error);
			result.success = false;
		} else {
			response = www.downloadHandler.text;
			Debug.Log ("SUCCESS " + response);
			result.success = true;
		}

		result.text = response;
	}
	public IEnumerator downloadFile(string path, String progress) {
		string request = baseURL + path;
		Debug.Log ("requested file is:" + request); 
		yield return StartCoroutine(downloadFileRoutine(request, progress));
	}

	private IEnumerator downloadFileRoutine(string url, String progress)
	{				
		WWW www = new WWW(url);
		//yield return www;

		while (!www.isDone) {
			progress = "downloaded " + (www.progress*100).ToString() + "%...";
			Debug.Log (progress);
			yield return null;
		}
		string fullPath = "e:/zipped_file.zip";
		File.WriteAllBytes (fullPath, www.bytes);
		Debug.Log ("text: "+www.text);
		Debug.Log ("error: "+www.error);
		progress = "downloaded, unzipping...";
	}

	public void setAuthParams(string u, string loginid){
		username = u;
		loginID = loginid;
		Auth = "Auth="+loginID + "," + u;
	}

	public void MyExtract(string zipToUnpack, string unpackDirectory)
	{
		using (ZipFile zip1 = ZipFile.Read(zipToUnpack))
		{
			foreach (ZipEntry e in zip1)
			{
				e.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);
			}
		}
	}
	public void setLoginResponse(string response){
		loginResponse = response.Split (',');
		int badgeLocation = findStringIndexInArray (loginResponse, "badgeLog");
		int examLocation = findStringIndexInArray (loginResponse, "examLog");
		//updating badges' status:
		for (int i = badgeLocation + 2; i < examLocation && !loginResponse[i].Equals(""); i++) {
			badges.Add (Convert.ToInt32(loginResponse [i]));
//			Debug.Log (loginResponse [i]);
		}

	}
	/**
	 * Returns if user has badge with given id or not
	 * */
	public bool hasBadge(int i){
		foreach (int badgeId in badges) {
			if (badgeId == i)
				return true;
		}
		return false;
	}
	private int findStringIndexInArray(String[] arr, string str){
		for (int i = 0; i < arr.Length; i++) {
			if(arr[i].Equals(str))
				return i;
		}
		return -1;	
	}
	public static bool searchItemInArray(string[] arr, string item){		
		foreach (string it in arr) {
			if (it.Equals (item))
				return true;
		}
		return false;
	}	
}
