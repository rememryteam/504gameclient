﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class sceneManager : MonoBehaviour {
	int type;//total=1, league=2, recent=3
	bool friends = false;//determines whether we request friends leaderboard or global one.
	RowContentHolder rch;
	// Use this for initialization
	IEnumerator Start () {
		rch = GameObject.FindObjectOfType<RowContentHolder> ();
		friends = true;
		yield return rch.GetLeaderboard ("total",(friends)?"1":"0");
	}	
	public void leaderboardChangeHandler(string direction){		
		rch.loadingPanel.SetActive (true);
		rch.loadingPanel.GetComponentInChildren<Button> ().interactable = false;
		rch.loadingPanel.GetComponentInChildren<Text> ().text = "Getting data from server";
		foreach(Image image in rch.loadingPanel.GetComponentsInChildren<Image> ()){
			if (image.name.Equals ("animatedImage")) {
				image.enabled = true;
			}
		}
		if (direction.Equals ("left")) {
			type = (type + 2) % 3;//(type+2)%3 is equal to (type-1)%3 but it never returns -1
			StartCoroutine (rch.GetLeaderboard (getTypeString (type), (friends)?"1":"0"));
		} else if (direction.Equals ("right")) {
			type = (type + 1) % 3;
			Debug.Log ("getting new lb info " + getTypeString (type));
			StartCoroutine (rch.GetLeaderboard (getTypeString (type), (friends)?"1":"0"));
		} else { 			
			Debug.Log ("getting new lb info " + getTypeString (type));
			StartCoroutine (rch.GetLeaderboard (getTypeString (type), (friends)?"1":"0"));	
		}
	}
	IEnumerator changeLeaderboard(string ltype, string f){		
		yield return rch.GetLeaderboard (ltype,f);
	}
	public string getTypeString(int type){
		if (type == 0)
			return "total";
		else if (type == 1)
			return "myLeague";
		else if (type == 2)
			return "lastTimePeriod";
		return "wtf";//should never happen
	}

	public void userClicked(Button btn){
		codeMother.cmInstance.friend.name = btn.GetComponentsInChildren<Text> () [1].text;
		codeMother.cmInstance.friend.score = btn.GetComponentsInChildren<Text> () [2].text;
		codeMother.cmInstance.friend.level = btn.GetComponentsInChildren<Text> () [3].text;
		codeMother.cmInstance.friend.id = btn.GetComponentsInChildren<Text> () [4].text;
		codeMother.cmInstance.friend.IFollowHim = btn.GetComponentsInChildren<Button> () [1].image.color.Equals(Color.green);
		Debug.Log ("You want to see user profile with id= "+ codeMother.cmInstance.friend);	
		SceneManager.LoadScene ("profileScene");
	}

	public void followFromList(Button button){
		string whatToDo = (button.GetComponentsInChildren<Button>() [1].image.color.Equals(Color.green))?"unfollow":"follow";
		string userid = button.GetComponentsInChildren<Text>()[3].text;
		HttpResponse htre = new HttpResponse ();
		string[] parms = { "follower="+codeMother.cmInstance.myID,"followee="+userid};			
		Debug.Log(whatToDo+" "+ codeMother.cmInstance.myID+" "+userid);
		StartCoroutine(followFromListRoutine("Follow",whatToDo, parms, htre, button));
	}
	public void changeFriendsGlobal(Text t){
		friends = !friends;
		t.text = (friends) ? "GLOBAL LEADERBOARD" : "FRIENDS LEADERBOARD";
		leaderboardChangeHandler ("");
	}

	/**
	 * Depending on action, sends a follow or unfollow request
	*/
	public IEnumerator followFromListRoutine(string controller, string action, string[] parms, HttpResponse htre, Button button){
		yield return codeMother.cmInstance.sendHttpRequest (controller, action, parms, htre);
		if (htre.text.Equals ("0")) {
			Debug.Log (htre.text);	
			if (button.GetComponentsInChildren<Button> () [1].image.color.Equals(Color.green)){
				Debug.Log ("check unfollow of "+parms[0]+" and "+parms[1]);
				button.GetComponentsInChildren<Button> () [1].image.color = Color.white;
			} else {
				Debug.Log ("check follow of "+parms[0]+" and "+parms[1]);
				button.GetComponentsInChildren<Button> () [1].image.color= Color.green;
			}
		}
	}
}
