﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Text;
using UnityEngine.UI;
using Mono.Data.Sqlite;
using UnityEngine.SceneManagement;


public class questionHandler : MonoBehaviour {	
	HttpResponse httpResponse;
	public Text text1;
	public Text text2;
	public Text numberText;
	public string[] questions;
	private int questionNumber =1;
	private int[] userAnswers;
	public GameObject panel;
	public SqliteConnection con_db;
	public SqliteCommand cmd_db;
	public SqliteDataReader rdr;
	private string[] dimensonAnswers;//comma seperated string of answers per dimensoin. each item in these strings is 1 or 2, that is what server wants
	private string userResult, userTimes;
	private TimeSpan[] ts;
	private DateTime timeNow;
	private int gender=-1, age=100;
	// Use this for initialization
	void Start () {
		questions = new string[120];
		userAnswers = new int[60];
		ts = new TimeSpan[60];
		dimensonAnswers = new string[4];
		bool loaded = LoadQuestions ("Assets/Resources/questionnaire.txt");
//		cm = GameObject.FindObjectOfType<codeMother> ();
		Debug.Log(codeMother.cmInstance.Auth);
		timeNow = DateTime.Now;
		if (loaded) {
			showQuestion (questionNumber);
		}
	}
	private bool LoadQuestions(string fileName)
	{
		// Handle any problems that might arise when reading the text
		try
		{
			string line;
			// Create a new StreamReader, tell it which file to read and what encoding the file
			// was saved as
			StreamReader theReader = new StreamReader(fileName, Encoding.Default);
			int i=0;
			using (theReader)
			{
				// While there's lines left in the text file, do this:
				do
				{
					line = theReader.ReadLine();
					if (line != null)
					{						
						questions[i] = line;
						i++;
					}
				}
				while (line != null);
				// Done reading, close the reader and return true to broadcast success    
				theReader.Close();
				return true;
			}
		}
		// If anything broke in the try block, we throw an exception with information
		// on what didn't work
		catch (Exception e)
		{
			Debug.Log( e.Message);
			return false;
		}
	}
	public void showQuestion(int question){
		timeNow = DateTime.Now;
		int i = (question - 1) * 2;
		text1.text = questions [i];
		text2.text = questions [i + 1];
		numberText.text = question.ToString();
	}
	public void goNext(){
		if (questionNumber == 60) {
			numberText.text = getMBTIType (findUserType (userAnswers));
			con_db = codeMother.cmInstance.connectDB ();
			SqliteCommand command = new SqliteCommand ("update userInfo set \"personality\"= \""+numberText.text + "\" where id>0",con_db);
			Debug.Log (command.CommandText);
			rdr = command.ExecuteReader ();
			codeMother.cmInstance.disconnectDB (rdr, con_db, command);
			userResult = getUserResultParam (dimensonAnswers,numberText.text);
			userTimes = getUserTimesParam ();
			string[] parms = new string[3];
			parms [0] = "userResult=" + userResult;
			parms [1] = "userResultTime=" + userTimes;
			parms [2] = "" + codeMother.cmInstance.Auth;
			HttpResponse httpResponse = new HttpResponse ();
			StartCoroutine(login("Questionnaire", "Qresult", parms,httpResponse));
			return;
		}
		questionNumber++;
		showQuestion (questionNumber);
	}

	public void goPerv(){
		if (questionNumber == 1)
			return;
		questionNumber--;
		showQuestion (questionNumber);
	}
	public void answerClicked(int choice){
		userAnswers [questionNumber-1] = choice;
		TimeSpan temp = DateTime.Now -timeNow;
		ts [questionNumber-1] =  temp;
		Debug.Log ("on question " + questionNumber + " user scored " + choice+", answer took this much seconds: "+temp.TotalMilliseconds);
		goNext ();
	}

	public int[] findUserType(int[] answers){
		int[] result = new int[4];
		for (int i = 0; i < 4; i++) {
			result [i] = 0;
			for (int j = i; j < 60; j += 4) {
				result [i] += answers [j];
				if (answers [j] == -1)
					dimensonAnswers [i] += "2,";
				if (answers [j] == 1)
					dimensonAnswers [i] += "1,";
			}
			Debug.Log("On dimension "+i+", user scored "+result[i]);
		}
		return result;
	}
	public string getMBTIType(int[] result){
		string type = "";
		if (result [0] < 0)
			type += "E";
		else
			type += "I";
		if (result [1] < 0)
			type += "N";
		else
			type += "S";
		if (result [2] < 0)
			type += "F";
		else
			type += "T";
		if (result [3] < 0)
			type += "J";
		else
			type += "P";
		return type;				
	}
	public string getUserResultParam(string[] dims, string type){
		string result = "";
		for (int j = 0; j < 4; j++) {
			result += (j+1) + ",";
			result += dims [j];
			result += type.ToCharArray () [j];
			if (j != 3)
				result += ",";
		}
		return result+","+age+","+gender;
	}
	public string getUserTimesParam(){
		string result = "";
		for (int i = 0; i < 59; i++)
			result += Convert.ToInt32(ts[i].TotalMilliseconds)/100+",";
		result += ""+Convert.ToInt32(ts[59].TotalMilliseconds/100);
		return result;
	}
	/**
	 * 0 = female, 1= male
	 * */
	public void selectGender(Button x){
		if (x.name.Equals ("FrauButton"))
			gender = 0;
		else
			gender = 1;
		Debug.Log (gender);
	}
	public void showQuestionnaire(){
		if (gender == -1)
			Debug.Log ("tell him to choose gender");
		else if (age == 0)
			Debug.Log ("tell him to choose age");
		else {
			panel.SetActive (false);
		}
	}		
	private IEnumerator login(string controller, string action, string[] ps, HttpResponse htre){
		yield return codeMother.cmInstance.sendHttpRequest (controller, action, ps, htre);
		codeMother.cmInstance.loginResponse = htre.text.Split(',');
		SceneManager.LoadScene ("levelsScene");		
	}
}
