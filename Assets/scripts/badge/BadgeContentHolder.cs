﻿using UnityEngine;
using System.Collections;
using System;

[System.Serializable]
public class BadgeContent{
	public int rank, level, score,id;
	public string name;
	public bool me = false;
	public override string ToString(){
		return "rank: " + rank + ", level: " + level + ", score: " + score + ", id: " + id + ", name: " + name;
	}
}
public class BadgeContentHolder : MonoBehaviour {
	public BadgeContent[] rows;
	private HttpResponse httpResponse;
	private codeMother cm;
	private int myRank, myLevel, myScore;
	private string myName;
	public string lbTitle;
	/**
	 * This function sends an http request to get leaderboard data and loads the data to "rows" array after parsing them
	 * "ltype" must be on of these: {total, league, }
	 * "friends" parameter must be zero or one
	 * */
	public IEnumerator GetLeaderboard(string ltype, string friends) { 
		Debug.Log ("starting BadgeContentHolder");
		cm = GameObject.FindObjectOfType<codeMother> ();
		string[] parameters = new string[3];
		parameters [0] = cm.Auth;
		parameters [1] = "ltype="+ltype;
		parameters [2] = "friends=" + friends;
		httpResponse = new HttpResponse ();
		yield return cm.sendHttpRequest ("Leaderboard", "getLeaderboardWithFriends", parameters,httpResponse);
		Debug.Log (httpResponse.text);
		string[] rawResponseArray = httpResponse.text.Split (',');
		int usersInLeaderboard = (rawResponseArray.Length - 6) / 4;
		myRank = Convert.ToInt32(rawResponseArray [4]);
		myName = rawResponseArray [2];
		myLevel = Convert.ToInt32(rawResponseArray [3]);
		myScore = Convert.ToInt32(rawResponseArray [3]);
		int rank = 1, j=0;
		rows = new BadgeContent[usersInLeaderboard];
		for (int i = 5; i < rawResponseArray.Length; i++) {
			try{
				BadgeContent row = new BadgeContent ();
				row.rank = rank++;
				row.id = Convert.ToInt32 (rawResponseArray [i++]);
				row.name = (rawResponseArray [i++]);
				row.score = Convert.ToInt32 (rawResponseArray [i++]);
				row.level = Convert.ToInt32 (rawResponseArray [i]);
				Debug.Log (row.ToString ());
				rows[j++]=row;
			}catch(FormatException x){
				Debug.Log ("exception on i=" + i + " where length is: " + rawResponseArray.Length+x);
			}
		}
		if(myRank>usersInLeaderboard){
			BadgeContent row = new BadgeContent ();
			row.rank = myRank;
			row.id = Convert.ToInt32(cm.myID);
			row.name = myName;
			row.score = myScore;
			row.level = myLevel;
			row.me = true;
			rows[usersInLeaderboard-1]=row;
		}
		lbTitle = getLBTitle (ltype);
		GameObject.FindObjectOfType<RowPlacer> ().PlaceRows();
	}
	 
	public bool hasSucceeded(){
		return httpResponse.success;
	}
	public string getLBTitle(string ltype){
		if (ltype.Equals ("total"))
			return "TOTAL";
		if (ltype.Equals ("myLeague"))
			return "LEAGUE";
		if (ltype.Equals ("lastTimePeriod"))
			return "LAST 3 DAYS";
		return "wtf";//should never happen
	}
}
