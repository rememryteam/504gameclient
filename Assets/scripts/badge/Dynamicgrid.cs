﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/**
 * This class is attached to the scorrable panel and fits three square badges in it in every row
 * */
public class Dynamicgrid : MonoBehaviour {
	public int col;
	// Use this for initialization
	void Start () {
		col = 5;
		RectTransform parent = gameObject.GetComponent<RectTransform> ();
		GridLayoutGroup grid = gameObject.GetComponent<GridLayoutGroup> ();
		grid.cellSize = new Vector2 (parent.rect.width / col, parent.rect.width / col);
	}
}
