﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Mono.Data.Sqlite;
using System.Data;
using System;
using UnityEngine.SceneManagement;

public class settings : MonoBehaviour {
//	public enum sliderValue {game, card}
	public Image gameVolumeImage;
	public Image cardVolumeImage;
	public Slider gameSlider;
	public Slider cardSlider;
	public Sprite[] volumeSprites;
	public Toggle[] toggles;

	public SqliteConnection con_db;
	public SqliteCommand cmd_db;
	public SqliteDataReader rdr;
	private string dbPath;
	private bool startPhase = true;

	private codeMother cm;
	// Use this for initialization: we read vurrent state of settings from the db and load the page
	void Start () {
		cm = GameObject.FindObjectOfType<codeMother> ();
		Debug.Log ("start starting!");
		con_db = codeMother.cmInstance.connectDB ();
		SqliteCommand command = new SqliteCommand ("Select * from userInfo",con_db);
		rdr = command.ExecuteReader ();
		Debug.Log ("command executed, length is: "+rdr.FieldCount);

		if (rdr.Read ()) {
			float card = (float) rdr.GetDouble(2);
			float game = (float) rdr.GetDouble(3);
			Debug.Log ("game: "+game+", card: "+card);
			cardSlider.value = card;
			gameSlider.value = game;
			changeImage (cardVolumeImage, card);
			changeImage (gameVolumeImage, game);
		}
	    command = new SqliteCommand ("Select * from feedbacks",con_db);
		rdr = command.ExecuteReader ();
		int i = 0;
		while (rdr.Read ()) {
			bool turnedOff = rdr.GetInt32 (1)!=0;
			bool canTurnOff = rdr.GetInt32 (2)!=0;
			Debug.Log ("On feedback id " + (i+1) + ", canTurnOff is " + canTurnOff + ", turnOff is " + turnedOff);
			if (!canTurnOff)
				toggles [i].interactable = false;
			if (turnedOff)
				toggles [i].isOn = false;
			i++;
		}
		con_db.Close ();
		startPhase = false;
	}
		
	public void onVolumeChange(Slider s){		
		if (startPhase)
			return;
		float val = s.value;
		Image image = null;
		if (s.name.Equals ("cardSlider")) {
			image = cardVolumeImage;
		}
		else if(s.name.Equals ("gameSlider")) {
			image = gameVolumeImage;
		}
		changeImage (image, val);
	}

	/**
	 * The following function changes the sprite of Image as the value given changes
	*/
	public void changeImage(Image image, float val){			
		Sprite spr;
		if (val < 0.01)
			spr = volumeSprites [0];
		else if (val < 0.3)
			spr = volumeSprites [1];
		else if (val < 0.65)
			spr = volumeSprites [2];
		else
			spr = volumeSprites [3];
		image.sprite = spr;
	}
	/**
	 * Saves the changes user has made to local and server db
	 * */
	public void saveChanges(){
		//volume:
		con_db = codeMother.cmInstance.connectDB ();
		SqliteCommand command = new SqliteCommand ("update userInfo set \"cardVolume\"= "+cardSlider.value+", \"gameVolume\"= "+gameSlider.value+" where id>0",con_db);
		Debug.Log ("update userInfo set \"cardVolume\"= "+cardSlider.value+", \"gameVolume\"= "+gameSlider.value+" where id>0");
		rdr = command.ExecuteReader ();
		codeMother.cmInstance.disconnectDB (rdr, con_db, command);
		//feedbacks:
		HttpResponse httr = new HttpResponse();
		string[] parameters = new string[4];
		parameters [0] = "deviceId="+codeMother.cmInstance.loginID;
		parameters [1] = "username="+codeMother.cmInstance.username;
		int i = 1;
		foreach (Toggle t in toggles) {
			parameters [2] = "fid="+i;
			parameters [3] = "status="+Convert.ToInt32(!t.isOn);
			StartCoroutine(sendFeedbacksToServer("Feedback", "setFeedbackOnOff", parameters,httr));
			i++;
		}
		con_db = codeMother.cmInstance.connectDB ();
		i = 1;
		SqliteCommand commandToggles=null;
		foreach (Toggle t in toggles) {
			commandToggles = new SqliteCommand ("update feedbacks set \"turnedOff\"= "+Convert.ToInt32(!t.isOn)+" where id="+i,con_db);
			Debug.Log (commandToggles.CommandText);
			rdr = commandToggles.ExecuteReader ();
			i++;
		}
		codeMother.cmInstance.disconnectDB (rdr, con_db, commandToggles);
		SceneManager.LoadScene ("levelsScene");	
	}

	public IEnumerator sendFeedbacksToServer(string controller, string action, string[] parms, HttpResponse httr){
		yield return codeMother.cmInstance.sendHttpRequest(controller, action, parms,httr);

	}
}