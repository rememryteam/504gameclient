﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class HUDManager : MonoBehaviour {

	// Use this for initialization

	public void settingsClicked(){
		SceneManager.LoadScene ("settingsScene");	
	}
	public void leaderboardClicked(){
		SceneManager.LoadScene ("leaderboardScene");	
	}
	public void badgesClicked(){
		SceneManager.LoadScene ("badgeScene");	
	}
}
