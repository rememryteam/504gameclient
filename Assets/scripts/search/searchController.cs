﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class searchController : MonoBehaviour {
	public InputField searchInput;
	public GameObject resultPanel;
	public Button itemPrefab;
	public User[] resultsUsers;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void submitSearch(){
		string query = searchInput.text;
		StartCoroutine (submitSearchRoutine(query));
	}
	public IEnumerator submitSearchRoutine(string query){
		HttpResponse htre = new HttpResponse ();
		string[] parms = {"name=" + query ,"username="+codeMother.cmInstance.username,"deviceId="+codeMother.cmInstance.loginID};
		yield return codeMother.cmInstance.sendHttpRequest ("follow", "searchUserByName", parms, htre);
		Debug.Log (htre.text);
		string response = htre.text;
		if (response.Equals ("-1")) {
			Button newItem = Instantiate (itemPrefab) as Button;
			newItem.GetComponentsInChildren<Text> () [0].text = "No users found";
			newItem.transform.SetParent (resultPanel.transform);
		} else {
			string[] userStrings = response.Split (';');
			List<User> users = new List<User> ();
			foreach (string userString in userStrings) {
				if (userString.Equals (""))
					break;
				string[] userItems = userString.Split (',');
				User user = new User (userItems [0], userItems [2], userItems [1], userItems [3], userItems [4].Equals ("1"));
				Debug.Log (user.ToString ());
				users.Add (user);
			}
			resultsUsers = users.ToArray ();
			RectTransform rowRectTransform = itemPrefab.GetComponent<RectTransform> ();
			RectTransform resultRectTransform = resultPanel.GetComponent<RectTransform> ();
			foreach (User result in resultsUsers) {
				Button newItem = Instantiate (itemPrefab) as Button;
				newItem.name = gameObject.name + "item at ";
				newItem.GetComponentsInChildren<Text> () [0].text = result.name;
				newItem.GetComponentsInChildren<Text> () [1].text = result.score;
				newItem.GetComponentsInChildren<Text> () [2].text = result.level;
				newItem.GetComponentsInChildren<Text> () [3].text = result.id;
				newItem.transform.SetParent (resultPanel.transform);
			}
		}
	}
	public void userClicked(Button btn){
		codeMother.cmInstance.friend.name = btn.GetComponentsInChildren<Text> () [0].text;
		codeMother.cmInstance.friend.score = btn.GetComponentsInChildren<Text> () [1].text;
		codeMother.cmInstance.friend.level = btn.GetComponentsInChildren<Text> () [2].text;
		codeMother.cmInstance.friend.id = btn.GetComponentsInChildren<Text> () [3].text;
		codeMother.cmInstance.friend.IFollowHim = btn.GetComponentsInChildren<Button> () [1].image.color.Equals(Color.green);
		Debug.Log ("You want to see user profile with id= "+ codeMother.cmInstance.friend);	
		SceneManager.LoadScene ("profileScene");
	}

	public void followFromList(Button button){
		string whatToDo = (button.GetComponentsInChildren<Button>() [1].image.color.Equals(Color.green))?"unfollow":"follow";
		string userid = button.GetComponentsInChildren<Text>()[3].text;
		HttpResponse htre = new HttpResponse ();
		string[] parms = { "follower="+codeMother.cmInstance.myID,"followee="+userid};			
		Debug.Log(whatToDo+" "+ codeMother.cmInstance.myID+" "+userid);
		StartCoroutine(followFromListRoutine("Follow",whatToDo, parms, htre, button));
	}

	/**
	 * Depending on action, sends a follow or unfollow request
	*/
	public IEnumerator followFromListRoutine(string controller, string action, string[] parms, HttpResponse htre, Button button){
		yield return codeMother.cmInstance.sendHttpRequest (controller, action, parms, htre);
		if (htre.text.Equals ("0")) {
			Debug.Log (htre.text);	
			if (button.GetComponentsInChildren<Button> () [1].image.color.Equals(Color.green)){
				Debug.Log ("check unfollow of "+parms[0]+" and "+parms[1]);
				button.GetComponentsInChildren<Button> () [1].image.color = Color.white;
			} else {
				Debug.Log ("check follow of "+parms[0]+" and "+parms[1]);
				button.GetComponentsInChildren<Button> () [1].image.color= Color.green;
			}
		}
	}
}